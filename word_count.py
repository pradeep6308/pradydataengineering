import random 
import os
import string
import sys
import codecs
import re
import itertools

stopWordsList = ["i", "me", "my", "myself", "we", "our", "ours", "ourselves", "you", "your", "yours",
            "yourself", "yourselves", "he", "him", "his", "himself", "she", "her", "hers", "herself", "it", "its",
            "itself", "they", "them", "their", "theirs", "themselves", "what", "which", "who", "whom", "this", "that",
            "these", "those", "am", "is", "are", "was", "were", "be", "been", "being", "have", "has", "had", "having",
            "do", "does", "did", "doing", "a", "an", "the", "and", "but", "if", "or", "because", "as", "until", "while",
            "of", "at", "by", "for", "with", "about", "against", "between", "into", "through", "during", "before",
            "after", "above", "below", "to", "from", "up", "down", "in", "out", "on", "off", "over", "under", "again",
            "further", "then", "once", "here", "there", "when", "where", "why", "how", "all", "any", "both", "each",
            "few", "more", "most", "other", "some", "such", "no", "nor", "not", "only", "own", "same", "so", "than",
            "too", "very", "s", "t", "can", "will", "just", "don", "should", "now",""]


def getIndexes(seed):
    random.seed(seed)
    n = 10000
    number_of_lines = 50000
    ret = []
    for i in range(0,n):
        ret.append(random.randint(0, 50000-1))
    return ret

def process(userID):
    indexes = getIndexes(userID)
    ret = []
    # TODO
                    
    for word in ret:
        print(word)

delimiters = " \t,;.?!-:@[](){}_*/"

def split_Word(word):
    word=word.strip()
    #word = re.sub(r"[\n\t\s/_/\-/(/)/{/}/@/,/;/!/./:/\*/\n/\t]",',',word)
    word=re.split(r'[/\]/\[/_/(/)/{/}/@/,/;/!/./\///\-/\n/\*/\t/\\/:/?/]',word)
    return word   

def read_file():
    f = codecs.open('input.txt','rb',"utf-8").readlines()
    word_list = []
    for i in f:
        word_list.append(split_Word(i))
    #print(word_list)
    word_list = [x for x in word_list if x is not None ]
    #print(word_list)
    word_list = list(itertools.chain(*word_list))
    word_list = [x.strip().lower() for x in word_list if x.lower() not in stopWordsList ]
    return word_list

def word_count(words):
    counts  = dict()
    indexes = getIndexes(1)
    indexed_data = [words[i] for i in indexes]
    for word in indexed_data:
        if word in counts:
            counts[word] += 1
        else:
            counts[word] = 1
    return counts

data=read_file()
word_cnt = word_count(data)

for k,v in sorted(word_cnt.items(),key=lambda words:(-words[1],words[0]))[:20]:
    print(k,v)


 

